<?php

namespace Exinent\Wholesale\Model\Customer\Attribute\Source;

class Status extends \Magento\Eav\Model\Entity\Attribute\Source\Table {

    public function getAllOptions() {
        if (!$this->_options) {
            $this->_options = Array(Array('value' => 1, 'label' => 'New'),
                Array('value' => 2, 'label' => 'Approve'),
                Array('value' => 3, 'label' => 'Reject'));
        }
        return $this->_options;
    }

}
