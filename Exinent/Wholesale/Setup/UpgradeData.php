<?php
namespace Exinent\Wholesale\Setup;

use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetup;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{

    /**
     * @var CustomerSetupFactory
     */
    private $customerSetupFactory;

    public function __construct(CustomerSetupFactory $customerSetupFactory)
    {
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $dbVersion = $context->getVersion();

        if (version_compare($dbVersion, '1.0.3', '<')) {
            /** @var CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
            $customerSetup->addAttribute(
                Customer::ENTITY,
                'accounts_payble_email_address',
                [
                    'label' => 'Accounts Payble Email Address',
                    'required' => 0,
                    'system' => 0, // <-- important, otherwise values aren't saved.
                                   // @see Magento\Customer\Model\Metadata\CustomerMetadata::getCustomAttributesMetadata()
                    'position' => 100
                ]
            );
            $customerSetup->getEavConfig()->getAttribute('customer', 'accounts_payble_email_address')
                ->setData('used_in_forms', ['customer_account_create', 'customer_account_edit', 'checkout_register', 'adminhtml_customer'])
                ->save();


            $customerSetup->addAttribute(
                Customer::ENTITY,
                'website_url',
                [
                    'label' => 'Website (Must have a URL)',
                    'required' => 0,
                    'system' => 0, // <-- important, otherwise values aren't saved.
                                   // @see Magento\Customer\Model\Metadata\CustomerMetadata::getCustomAttributesMetadata()
                    'position' => 100
                ]
            );
            $customerSetup->getEavConfig()->getAttribute('customer', 'website_url')
                ->setData('used_in_forms', ['customer_account_create', 'customer_account_edit', 'checkout_register', 'adminhtml_customer'])
                ->save();

       
            $customerSetup->addAttribute(
                Customer::ENTITY,
                'date_founded',
                [
                    'label' => 'Date Founded',
                    'required' => 0,
                    'system' => 0, 
                    'input' => 'date',
                    'position' => 100
                ]
            );
            $customerSetup->getEavConfig()->getAttribute('customer', 'date_founded')
                ->setData('used_in_forms', ['customer_account_create', 'customer_account_edit', 'checkout_register', 'adminhtml_customer'])
                ->save();

            $customerSetup->addAttribute(
                Customer::ENTITY,
                'is_website_live',
                [
                    'label' => 'Is Website live?',
                    'required' => 0,
                    'system' => 0, 
                    'input' => 'select',
                    'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'position' => 100
                ]
            );
            $customerSetup->getEavConfig()->getAttribute('customer', 'is_website_live')
                ->setData('used_in_forms', ['customer_account_create', 'customer_account_edit', 'checkout_register', 'adminhtml_customer'])
                ->save();

        }
    }
}