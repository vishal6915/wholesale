<?php

namespace Exinent\Wholesale\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;

class wholesaleCustomerLogin implements ObserverInterface {

    protected $_customerRepository;
    protected $customerMapper;

    public function __construct(CustomerRepositoryInterface $customerRepository, \Magento\Customer\Model\Customer\Mapper $customerMapper) {
        $this->_customerRepository = $customerRepository;
        $this->customerMapper = $customerMapper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $customerObject = $observer->getEvent()->getModel();
//        $customer = $this->_customerRepository->getById($customerObject->getEntityId());
//        $customerData = $this->customerMapper->toFlatArray($customer);
        if ($customerObject->getGroupId() == 2 && $customerObject->getWholesaleStatus() != 2) {
            $message = $this->getErrorMessage($customerObject->getWholesaleStatus());
            throw new \Exception(__($message));
        }
        return TRUE;
    }

    private function getErrorMessage($status) {
        switch ($status){
        case 1:
            return 'Your Account is not Confirmed. please contact our representative';
        case 3:
            return 'Your Account is Suspended. please contact our representative';
        default:
            return 'Your Account is not Confirmed. please contact our representative';
        }
    }

}
