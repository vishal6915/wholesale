<?php

namespace Exinent\Wholesale\Observer;

use Magento\Framework\Event\ObserverInterface;

class wholesalePaymentCheck implements ObserverInterface {

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        if ($customerSession->isLoggedIn() && $customerSession->getCustomerGroupId() != 2) {
           $result = $observer->getEvent()->getResult();
            if ($observer->getEvent()->getMethodInstance()->getCode() == 'net30') {
                $result->setData('is_available', false);
            }
            return $result;
        }
    }

}
