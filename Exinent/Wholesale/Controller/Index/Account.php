<?php

namespace Exinent\Wholesale\Controller\Index;

class Account extends \Magento\Customer\Controller\Account\CreatePost {

    public function execute($coreRoute = null) {
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->session->isLoggedIn() || !$this->registration->isAllowed()) {
            $resultRedirect->setPath('*/*/');
            return $resultRedirect;
        }
        if (!$this->getRequest()->isPost()) {
            $url = $this->urlModel->getUrl('*/*/create', ['_secure' => true]);
            $resultRedirect->setUrl($this->_redirect->error($url));
            return $resultRedirect;
        }
        $this->session->regenerateId();
        try {
            if ($this->getRequest()->getParam('group_id') != 2) {
                return parent::execute($coreRoute);
            }
            $address = $this->extractAddress();
            $addresses = $address === null ? [] : [$address];
            $customer = $this->customerExtractor->extract('customer_account_create', $this->_request);
            $customer->setGroupId($this->getRequest()->getParam('group_id'));
            $customer->setData('is_active', 0);
            $customer->setAddresses($addresses);
            $password = $this->getRequest()->getParam('password');
            $confirmation = $this->getRequest()->getParam('password_confirmation');
            $redirectUrl = $this->session->getBeforeAuthUrl();
            $this->checkPasswordConfirmation($password, $confirmation);
            $customer = $this->accountManagement->createAccount($customer, $password, $redirectUrl);
            if ($this->getRequest()->getParam('is_subscribed', false)) {
                $this->subscriberFactory->create()->subscribeCustomerById($customer->getId());
            }
            $this->_eventManager->dispatch(
                    'customer_register_success', ['account_controller' => $this, 'customer' => $customer]
            );
            $confirmationStatus = $this->accountManagement->getConfirmationStatus($customer->getId());
            if ($this->getRequest()->getParam('group_id') != 2) {
                $this->session->setCustomerDataAsLoggedIn($customer);
                $this->messageManager->addSuccess($this->getSuccessMessage());
                $resultRedirect = $this->accountRedirect->getRedirect();
            } else {
                $this->messageManager->addSuccess($this->getSuccessMessage());
                $this->messageManager->addSuccess(
                        __(
                                'account created successfully, please contact adminisitrator to activate your account'
                        )
                );
//                $this->_objectManager->get('NameSpace\ModuleNmae\Helper\Email')->SendEmailMethod($emailTempVariables,$senderInfo,$receiverInfo,$type);
                $url = $this->urlModel->getUrl('*/*/index', ['_secure' => true]);
                $resultRedirect->setUrl($this->_redirect->success($url));
            }
            return $resultRedirect;
        } catch (StateException $e) {
            $url = $this->urlModel->getUrl('customer/account/forgotpassword');
            // @codingStandardsIgnoreStart
            $message = __(
                    'There is already an account with this email address. If you are sure that it is your email address, <a href="%1">click here</a> to get your password and access your account.', $url
            );
            // @codingStandardsIgnoreEnd
            $this->messageManager->addError($message);
        } catch (InputException $e) {
            $this->messageManager->addError($this->escaper->escapeHtml($e->getMessage()));
            foreach ($e->getErrors() as $error) {
                $this->messageManager->addError($this->escaper->escapeHtml($error->getMessage()));
            }
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('We can\'t save the customer.'));
        }
        return parent::execute($coreRoute);
    }

}
