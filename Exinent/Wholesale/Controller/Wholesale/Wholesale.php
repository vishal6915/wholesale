<?php

namespace Exinent\Wholesale\Controller\Wholesale;

class Wholesale extends \Magento\Framework\App\Action\Action {

    public function execute() {
        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }

}
